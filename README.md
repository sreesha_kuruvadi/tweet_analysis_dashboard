# tweet_analysis_dashboard

A simple web page where the user can analyze the tweet
A word cloud using d3.js is built in realtime from tweets which contains the search term.

`http://localhost:3000/{searchTerm}`

Project uses GCP NLP API and Stanford Core NLP API for sentiment analysis.

## Architecutre
![](https://bitbucket.org/sreesha_kuruvadi/tweet_analysis_dashboard/raw/b4a6c4a199424c53de358474874f27ece2ba4f84/backend/docs/architecture.png)


## Install `dep` - dependency management tool
- ### Mac-OS
  ```
  brew install dep
  ```

## [Create a Twitter Developer Account](https://developer.twitter.com/en.html)
- Create an application, generate `client secret`, `client id`, `access token` and `access secret` and add in `/backend/cmd/secrets/credentials/credentials.go`
   - var ClientID = ""
   - var ClientSecret = ""
   - var AccessToken = ""
   - var AccessSecret = ""

## [Setup StanfordNLP Core API Server](https://stanfordnlp.github.io/CoreNLP/corenlp-server.html#docker)
- Docker repository: https://hub.docker.com/r/frnkenstien/corenlp

```
docker pull frnkenstien/corenlp

docker run -p 9000:9000 --name coreNLP --rm -i -t frnkenstien/corenlp
```

## [Integrate GCP Go Library for NLP APIs](https://cloud.google.com/natural-language/docs/reference/libraries#client-libraries-install-go)
- Insert the json file with private key in `/backend/cmd/secrets/credentials/googleapi_nlp_secret.json`
- add the GCP projectID as `var GCPNLPProjectId = ""` in `/backend/cmd/secrets/credentials/credentials.go`

## Configuring Stanford Core NLP API or GCP API to be used
`NLPLibType` by default is `NlpGcp` and hence GCP NLP API is used.
This can be changed by setting `NLPLibType` to `NlpStanford` which uses stanford core NLP API.

```
//Service ...
type Service struct {
	Workers        int
	ResponseStream chan *TwitterAnalytics
	TweetStats     *TwitterAnalytics
	NLPLibType     NLPLibType
}
```

## Start Server
```
cd backend
dep ensure
go run cmd/main.go
```

## Start Browser Client

``` 
cd clients/browser-client
npm install
npm start
```

## Future prospects
### Backend
- Integrate socket.io
- Integrate Apache spark to stream tweets and build a data processing pipeline
- Compute word count on the server side using Apache spark
### Front End
- Provide a graphical timeline of most popular tweets along with their sentiment scores
- Provide a geographical visualization using heat maps based on sentiments
