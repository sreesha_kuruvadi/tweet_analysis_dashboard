import React from 'react';
import WordCloud from 'react-d3-cloud';

let { backend: { host, streamHost, tweetsSearchPath, tweetsStreamPath } } = require('./config/production.json')

var ws


class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      realtimeText_string: "",
      sentiment: {},
      tweets: []
    };
  }

  componentDidMount() {
    let queryString = (window.location.pathname.split("/") || []).join("")
    let searchUrl = host + tweetsSearchPath + "?q=" + queryString
    let streanUrl = streamHost + tweetsStreamPath + "?q=" + queryString

    if (!ws) {

      ws = new WebSocket(streanUrl);

      ws.onmessage = (evt) => {

        let data = JSON.parse(evt.data)

        if (data.type == "tweet") {
          this.setState({
            realtimeText_string: this.state.realtimeText_string + data.tweet.text
          })
        } else if (data.type == "sentiment") {
          this.setState({
            sentiment: data.tweetSentiment
          })
        }
        //console.log("RESPONSE: " + data.type == "tweet", data);

      }

      ws.onopen = (evt) => console.log("OPEN")
      ws.onclose = (evt) => {
        console.log("CLOSE");
        ws = null;
      }
      ws.onerror = (evt) => console.log("ERROR: " + evt.data)
    }

    fetch(searchUrl, {
      headers: new Headers({
        'Connection': 'upgrade',
      })
    })
      .then(res => res.json())
      .then(
        (serverData) => {
          console.log(serverData)
          let text_string = ""
          for (let i = 0; i < serverData.tweets.length; i++) {
            text_string += serverData.tweets[i].text
          }
          this.setState({ tweets: serverData.tweets, realtimeText_string: text_string, streanUrl, searchUrl })
        },
        (error) => {
          this.setState({ error: error.toString(), realtimeText_string: "", streanUrl, searchUrl })
        }
      )
  }

  componentWillUnmount() {
    ws.send("close")
    ws.close()
  }

  render() {
    return (
      <div>
        <h1> {"Realtime word cloud"}</h1>
        <pre id="json"> {"Average Sentiment :"+ this.state.sentiment.averageSentiment + "\n Sentiment Count : "+ JSON.stringify(this.state.sentiment.sentimentCount) } </pre>

        <WordCloud
          data={getText(this.state.realtimeText_string)}
          fontSizeMapper={fontSizeMapper}
        />
        {this.state.error || ""}
        <h1> {"Top Tweets"}</h1>
        <ul>
          {this.state.tweets.map(tweet=>{
            return <li color="red"> <div> {tweet.text} <br/> <b>{tweet.sentimentScore}</b> </div></li>
          })}
        </ul>

      </div>
    )
  }
}

const fontSizeMapper = word => Math.log2(word.value) * 20;

function getText(text_string) {
  var word_count = {};

  var words = text_string.split(/[ '\-\(\)\*":;\[\]|{},.!?]+/) || [];

  if (words.length == 1) {
    word_count[words[0]] = 1;
  } else {
    words = words.map((word) => {
      word = word.replace("https", " ")
      word = word.replace("//t", " ")
      word = word.replace("#", " ")
      word = word.trim()
      return word
    })

    words.forEach(function (word) {
      var word = word.toLowerCase();
      if (word != "") {
        if (word_count[word]) {
          word_count[word]++;
        } else {
          word_count[word] = 1;
        }
      }
    })

  }

  let result = []
  for (var key in word_count) {
    result.push({
      text: key,
      value: word_count[key]
    })
  }
  return result
}

export default App;
