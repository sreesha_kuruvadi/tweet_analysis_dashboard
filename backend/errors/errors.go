package errors

//CustomError ...
type CustomError struct {
	Message    string `json:"message"`
	StatusCode int    `json:"statusCode"`
}

//Error ...
func (e CustomError) Error() string {
	return e.Message
}

//NewCustomError ...
func NewCustomError(message string, statusCode int) CustomError {
	return CustomError{
		Message:    message,
		StatusCode: statusCode,
	}
}
