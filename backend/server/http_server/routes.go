package httpserver

import (
	"net/http"

	"bitbucket.org/tweet_analysis_dashboard/backend/server/http_server/handlers"
	service "bitbucket.org/tweet_analysis_dashboard/backend/service"
	"github.com/go-chi/chi"
	"github.com/rs/cors"
)

//HTTPRoutes ...
var HTTPRoutes = []EndPoint{
	EndPoint{
		Path:    "/search",
		Method:  http.MethodGet,
		Handler: handlers.SearchTweets,
	},
}

//WebSocketRoutes ...
var WebSocketRoutes = []EndPoint{
	EndPoint{
		Path:    "/tweets/stream",
		Handler: handlers.StreamTweets,
	},
}

//LoadRoutes ...
func (s *Server) LoadRoutes(enPoints []EndPoint) *Server {

	for _, endPoint := range enPoints {

		if endPoint.Method == "" {
			path, handler := endPoint.Wrap(s.ApplicationService)
			s.Router.HandleFunc(path, handler)
			continue
		}

		s.Router.Group(func(r chi.Router) {

			cors := cors.New(cors.Options{
				// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
				AllowedOrigins: []string{"*"},
				// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
				AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
				AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
				ExposedHeaders:   []string{"Link"},
				AllowCredentials: true,
				MaxAge:           300, // Maximum value not ignored by any of major browsers
			})

			r.Use(cors.Handler)

			path, handler := endPoint.Wrap(s.ApplicationService)
			r.MethodFunc(endPoint.Method, path, handler)
		})
	}
	return s
}

//Wrap ...
func (eP *EndPoint) Wrap(applicationService *service.Service) (string, http.HandlerFunc) {
	httpHandler := func(w http.ResponseWriter, r *http.Request) {
		// inject application based dependencies
		// cache connections; db connections; server-server connections
		eP.Handler(w, r, applicationService)
	}
	return eP.Path, httpHandler
}

//ApplicationHandlerFunc ...
type ApplicationHandlerFunc func(w http.ResponseWriter, r *http.Request, s *service.Service)
