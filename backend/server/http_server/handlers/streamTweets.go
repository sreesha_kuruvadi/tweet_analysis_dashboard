package handlers

import (
	"fmt"
	"net/http"
	"sync"

	"bitbucket.org/tweet_analysis_dashboard/backend/api"
	"bitbucket.org/tweet_analysis_dashboard/backend/api/analytics"
	"bitbucket.org/tweet_analysis_dashboard/backend/constants"
	"github.com/gorilla/websocket"

	service "bitbucket.org/tweet_analysis_dashboard/backend/service"
)

//StreamTweets ...
func StreamTweets(w http.ResponseWriter, r *http.Request, s *service.Service) {

	queryString := r.URL.Query().Get("q")
	tweetStream := make(chan api.TweetData, 0)
	analyticsTweetStream := make(chan api.TweetData, 0)
	socketChannel := make(chan interface{}, 0)

	c, err := s.API.Echo(w, r)
	if err != nil {
		fmt.Println(err)
	}

	stream, _ := s.API.Stream(tweetStream, r)
	defer stream.Stop()

	safeSocket := SafeSocket{Connection: c}

	safeSocket.Connection.SetCloseHandler(func(c int, text string) error {
		close(tweetStream)
		stream.Stop()
		return nil
	})

	analyticsService := analytics.NewService(
		&analytics.TwitterAnalytics{
			ID: queryString,
		},
		analytics.NlpGcp,
	)

	go func() {
		_, message, err := safeSocket.Connection.ReadMessage()
		if err != nil {
			fmt.Println(err)
		}
		if string(message) == "close" {
			fmt.Println("Closing result channel")
			close(tweetStream)
		}
	}()

	analyticsService.Analyze(r.Context(), analyticsTweetStream)

	go func() {
		for update := range analyticsService.ResponseStream {
			socketChannel <- SocketMessage{
				Type: constants.MessageTypeSentiment,
				TweetSentiment: &SocketSentimentData{
					ID:               queryString,
					AverageSentiment: update.AverageSentiment,
					SentimentCount:   update.SentimentCount,
				},
			}
		}
	}()
	go func() {
		for tweet := range tweetStream {
			// Send the tweet for sentiment analysis
			analyticsTweetStream <- tweet
			fmt.Println("Tweet Update", len(tweet.Text))
			socketChannel <- SocketMessage{
				Type:  constants.MessageTypeTweet,
				Tweet: &tweet,
			}
		}

	}()

	for data := range socketChannel {
		err = safeSocket.Connection.WriteJSON(data)
		if err != nil {
			fmt.Println(err)
		}
	}

	fmt.Println("exiting stream http handler")
}

//SafeSocket ...
type SafeSocket struct {
	Connection *websocket.Conn
	Mutex      sync.Mutex
}

//SocketMessage ...
type SocketMessage struct {
	Type           string               `json:"type"`
	Tweet          *api.TweetData       `json:"tweet"`
	TweetSentiment *SocketSentimentData `json:"tweetSentiment"`
}

//SocketSentimentData ...
type SocketSentimentData struct {
	ID               string             `json:"id"`
	AverageSentiment float64            `json:"averageSentiment"`
	SentimentCount   api.SentimentCount `json:"sentimentCount"`
}
