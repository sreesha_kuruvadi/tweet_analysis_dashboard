package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/tweet_analysis_dashboard/backend/api/analytics"
	service "bitbucket.org/tweet_analysis_dashboard/backend/service"
)

//SearchTweets ...
func SearchTweets(w http.ResponseWriter, r *http.Request, s *service.Service) {
	queryString := r.URL.Query().Get("q")
	countString := r.URL.Query().Get("c")

	if countString == "" {
		countString = "0"
	}

	count, err := strconv.Atoi(countString)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	result, err := s.API.GetTweets(queryString, count)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	analyticsService := analytics.NewService(
		&analytics.TwitterAnalytics{
			ID: queryString,
		},
		analytics.NlpGcp,
	)
	result.Tweets = analyticsService.GetTweetsWithSentiments(r.Context(), result.Tweets)

	byteData, _ := json.Marshal(result)
	w.Write(byteData)
}
