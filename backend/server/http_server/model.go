package httpserver

//EndPoint ...
type EndPoint struct {
	Path    string
	Method  string
	Handler ApplicationHandlerFunc
}
