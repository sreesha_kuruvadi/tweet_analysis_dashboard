package httpserver

import (
	"fmt"
	"net/http"

	applicationservice "bitbucket.org/tweet_analysis_dashboard/backend/service"
	"github.com/go-chi/chi"
)

//Server ...contains server configurations
type Server struct {
	Router             chi.Router
	ApplicationService *applicationservice.Service
}

//New ...creates a new server with the desired configurations
func New(s *applicationservice.Service) *Server {
	return &Server{
		Router:             chi.NewRouter(),
		ApplicationService: s,
	}
}

//Start ...starts the server with the appropriate configurations
func (s *Server) Start(address string) {
	fmt.Println("Starting server on" + address)
	http.ListenAndServe(address, s.Router)
}
