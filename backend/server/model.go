package server

import httpServer "bitbucket.org/tweet_analysis_dashboard/backend/server/http_server"

//Server ...
type Server struct {
	HTTPServer *httpServer.Server
}
