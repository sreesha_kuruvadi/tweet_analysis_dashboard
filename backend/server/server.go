package server

import (
	httpServer "bitbucket.org/tweet_analysis_dashboard/backend/server/http_server"
	applicationservice "bitbucket.org/tweet_analysis_dashboard/backend/service"
)

//New ...
func New(s *applicationservice.Service) *Server {
	return &Server{
		HTTPServer: httpServer.New(s),
	}
}

//StartHTTPServer ...
func (s *Server) StartHTTPServer(routes []httpServer.EndPoint, address string) {
	s.HTTPServer.LoadRoutes(routes).Start(address)
}
