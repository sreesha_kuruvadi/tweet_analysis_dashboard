package main

import (
	httpServer "bitbucket.org/tweet_analysis_dashboard/backend/server/http_server"

	"bitbucket.org/tweet_analysis_dashboard/backend/api"
	"bitbucket.org/tweet_analysis_dashboard/backend/cmd/secrets"
	"bitbucket.org/tweet_analysis_dashboard/backend/lib"
	"bitbucket.org/tweet_analysis_dashboard/backend/server"
	service "bitbucket.org/tweet_analysis_dashboard/backend/service"
	"github.com/dghubble/go-twitter/twitter"
)

var tokenEndPoint = "https://api.twitter.com/oauth2/token"

func main() {

	tSearchClient, tStreamClient := initTwitterClients()

	// api service contains dependencies required by each feature
	apiService := api.NewService(tSearchClient, tStreamClient)

	// creates an application level struct which stores dependency objects
	// db connections; server-server connections
	appService := service.New(apiService)

	// starts a http server for serving http requests
	go server.New(appService).StartHTTPServer(httpServer.HTTPRoutes, ":8090")

	// starts a http server to streaming tweets using websockets
	server.New(appService).StartHTTPServer(httpServer.WebSocketRoutes, ":8091")
}

func initTwitterClients() (*twitter.Client, *twitter.Client) {

	clientID, clientSecret, accessToken, accessSecret := secrets.GetCredentials()

	tSearchClient := lib.TwitterSearchClient(clientID, clientSecret, tokenEndPoint)
	tStreamClient := lib.TwitterStreamClient(clientID, clientSecret, accessToken, accessSecret)

	return tSearchClient, tStreamClient
}
