package lib

import (
	"net/http"
	"sync"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
)

var twitterSearchOnce sync.Once
var twitterStreamOnce sync.Once

var searchClient *twitter.Client
var streamClient *twitter.Client

//TwitterSearchClient ...creates a twitter search client
func TwitterSearchClient(clientID, clientSecret, tokenEndPoint string) *twitter.Client {
	twitterSearchOnce.Do(func() {
		// http.Client will automatically authorize Requests
		httpClient := GetOAuth2Client(clientID, clientSecret, tokenEndPoint)
		// Twitter client
		searchClient = twitter.NewClient(httpClient)
	})
	return searchClient
}

//TwitterStreamClient ...
func TwitterStreamClient(clientID, clientSecret, accessToken, accessSecret string) *twitter.Client {
	twitterStreamOnce.Do(func() {
		streamClient = NewStreamClient(clientID, clientSecret, accessToken, accessSecret)
	})
	return streamClient
}

//NewStreamClient ...creates a new twitter stream client
func NewStreamClient(clientID, clientSecret, accessToken, accessSecret string) *twitter.Client {
	// http.Client will automatically authorize Requests
	httpClient := GetOAuth1Client(clientID, clientSecret, accessToken, accessSecret)
	// Twitter client
	streamClient = twitter.NewClient(httpClient)
	return streamClient
}

//GetOAuth2Client ...creates a new oauth2 client
func GetOAuth2Client(clientID, clientSecret, tokenEndPoint string) *http.Client {
	clientCredentials := &clientcredentials.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		TokenURL:     tokenEndPoint,
	}
	return clientCredentials.Client(oauth2.NoContext)
}

//GetOAuth1Client ...creates a new oauth1 client
func GetOAuth1Client(clientID, clientSecret, accessToken, accessSecret string) *http.Client {
	config := oauth1.NewConfig(clientID, clientSecret)
	token := oauth1.NewToken(accessToken, accessSecret)
	// OAuth1 http.Client will automatically authorize Requests
	return config.Client(oauth1.NoContext, token)
}
