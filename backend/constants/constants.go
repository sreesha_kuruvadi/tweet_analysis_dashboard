package constants

/*
	Add application based constants here
*/
const (
	MessageTypeTweet     = "tweet"
	MessageTypeSentiment = "sentiment"
)
