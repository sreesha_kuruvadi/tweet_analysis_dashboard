package api

import (
	"fmt"
	"net/http"

	"bitbucket.org/tweet_analysis_dashboard/backend/cmd/secrets"
	"bitbucket.org/tweet_analysis_dashboard/backend/lib"
	"github.com/dghubble/go-twitter/twitter"
)

//Stream ...creates a twitter stream
func (s *Service) Stream(textChan chan<- TweetData, r *http.Request) (*twitter.Stream, error) {
	query := r.URL.Query().Get("q")

	// Convenience Demux demultiplexed stream messages
	demux := twitter.NewSwitchDemux()
	demux.Tweet = func(tweet *twitter.Tweet) {
		defer func() {
			if r := recover(); r != nil {
				fmt.Println(r)
			}
		}()
		textChan <- TweetData{
			Text: tweet.Text,
		}
	}

	filterParams := &twitter.StreamFilterParams{
		Track:         []string{query},
		StallWarnings: twitter.Bool(true),
	}

	clientID, clientSecret, accessToken, accessSecret := secrets.GetCredentials()
	streamCLient := lib.NewStreamClient(clientID, clientSecret, accessToken, accessSecret)

	stream, err := streamCLient.Streams.Filter(filterParams)
	if err != nil {
		fmt.Println(err)
	}

	// Receive messages until stopped or stream quits
	go demux.HandleChan(stream.Messages)
	return stream, err
}
