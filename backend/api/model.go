package api

//Data ...
type Data struct {
	Tweets []TweetData `json:"tweets"`
}

//TweetData ...
type TweetData struct {
	Text           string  `json:"text"`
	SentimentScore float64 `json:"sentimentScore"`
}

//SentimentCount ...
type SentimentCount struct {
	Total    float64 `json:"total,omitempty"`
	Negative float64 `json:"negative"`
	Positive float64 `json:"positive"`
	Neutral  float64 `json:"neutral"`
}
