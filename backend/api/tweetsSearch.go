package api

import (
	"fmt"

	"github.com/dghubble/go-twitter/twitter"
)

//GetTweets ...calls the tweet search API and returns the tweets associated
func (s *Service) GetTweets(searchTerm string, count int) (*Data, error) {
	//Search Tweets
	search, _, err := s.TwitterSearchClient.Search.Tweets(&twitter.SearchTweetParams{
		Query: searchTerm,
		Count: count,
	})
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	data := Data{
		Tweets: make([]TweetData, len(search.Statuses)),
	}

	for idx, tweet := range search.Statuses {
		data.Tweets[idx] = TweetData{
			Text: tweet.Text,
		}
	}

	return &data, nil
}
