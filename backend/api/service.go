package api

import (
	"github.com/dghubble/go-twitter/twitter"
)

//Service ...contains dependencies for all feature APIs
type Service struct {
	TwitterSearchClient *twitter.Client
	TwitterStreamClient *twitter.Client
}

//NewService ...creates a service with all feature API dependency references
func NewService(twitterSearchClient, twitterStreamClient *twitter.Client) *Service {
	return &Service{
		TwitterSearchClient: twitterSearchClient,
		TwitterStreamClient: twitterStreamClient,
	}
}
