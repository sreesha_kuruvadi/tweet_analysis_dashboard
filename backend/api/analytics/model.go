package analytics

import (
	"sync"

	"bitbucket.org/tweet_analysis_dashboard/backend/api"
)

//TwitterAnalytics ...contains sentiment data for a particualt date
type TwitterAnalytics struct {
	ID               string
	AverageSentiment float64
	SentimentCount   api.SentimentCount
	Mutex            sync.Mutex
}
