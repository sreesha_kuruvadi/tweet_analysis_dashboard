package analytics

type NLPLibType string

const (
	NlpGcp      NLPLibType = "gcp_type"
	NlpStanford NLPLibType = "stanford_type"
)
