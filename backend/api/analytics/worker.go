package analytics

import (
	"context"
	"fmt"

	"bitbucket.org/tweet_analysis_dashboard/backend/api"
)

//worker ...listens to incoming tweets, gets the sentiment score and pushes an update
func (s *Service) worker(ctx context.Context, tweetsStream <-chan api.TweetData) {
	for {
		select {
		case <-ctx.Done():
			fmt.Println("Exiting Tweet Worker")
			return
		case t := <-tweetsStream:
			sentimentScore, err := s.GetSentimentScore(ctx, t.Text)
			if err != nil {
				fmt.Println(err)
			}

			if sentimentScore != nil {
				//Lock so that only one go routine can access the tweet stats object at a time
				s.TweetStats.Mutex.Lock()

				s.TweetStats.AverageUpdate(*sentimentScore)
				s.TweetStats.Incr(*sentimentScore)

				s.TweetStats.Mutex.Unlock()
				// send an update in overall sentiment score
				s.ResponseStream <- s.TweetStats
			}
		}
	}
}
