package analytics

import (
	"context"
	"fmt"

	"bitbucket.org/tweet_analysis_dashboard/backend/api"
	"bitbucket.org/tweet_analysis_dashboard/backend/api/analytics/lib"
)

//Analyze ...starts workers which call GCP NLP API and return an update score
func (s *Service) Analyze(ctx context.Context, tweetsStream chan api.TweetData) {
	for i := 0; i < s.Workers; i++ {
		go s.worker(ctx, tweetsStream)
	}
}

//GetTweetsWithSentiments ...returns sentiment scores for each tweet
func (s *Service) GetTweetsWithSentiments(ctx context.Context, tweets []api.TweetData) []api.TweetData {

	newTweetData := make([]api.TweetData, len(tweets))

	for idx, tweet := range tweets {
		sentimentScore, err := s.GetSentimentScore(ctx, tweet.Text)
		if err != nil {
			fmt.Println(err)
		}
		if sentimentScore != nil {
			tweet.SentimentScore = *sentimentScore
		}
		newTweetData[idx] = tweet
	}

	return newTweetData
}

//GetSentimentScore ...starts workers which call GCP NLP API and return an update score
func (s *Service) GetSentimentScore(ctx context.Context, text string) (*float64, error) {
	switch s.NLPLibType {
	case NlpGcp:
		return lib.GetSentimentScoreGCPNLP(ctx, text)
	default:
		return lib.GetSentimentScoreStanfordNLP(ctx, text)
	}
}

//NewService ...
func NewService(tweetStats *TwitterAnalytics, nlpLibType NLPLibType) *Service {
	return &Service{
		ResponseStream: make(chan *TwitterAnalytics, 0),
		Workers:        3,
		TweetStats:     tweetStats,
		NLPLibType:     nlpLibType,
	}
}

//Service ...
type Service struct {
	Workers        int
	ResponseStream chan *TwitterAnalytics
	TweetStats     *TwitterAnalytics
	NLPLibType     NLPLibType
}
