package lib

import (
	"context"
	"fmt"
	"sync"

	"bitbucket.org/tweet_analysis_dashboard/backend/cmd/secrets"
	language "cloud.google.com/go/language/apiv1"
	"google.golang.org/api/option"
	languagepb "google.golang.org/genproto/googleapis/cloud/language/v1"
)

//GetSentimentScoreGCPNLP ...
func GetSentimentScoreGCPNLP(ctx context.Context, text string) (*float64, error) {

	jsonSecretFilePath, projectID := secrets.GetGCPCredentials()

	// Creates a client.
	client, err := getLanguageClient(ctx, jsonSecretFilePath, projectID)
	if err != nil {
		fmt.Printf("Failed to create client: %v", err)
		return nil, err
	}

	// Detects the sentiment of the text.
	sentiment, err := client.AnalyzeSentiment(ctx, &languagepb.AnalyzeSentimentRequest{
		Document: &languagepb.Document{
			Source: &languagepb.Document_Content{
				Content: text,
			},
			Type: languagepb.Document_PLAIN_TEXT,
		},
		EncodingType: languagepb.EncodingType_UTF8,
	})
	if err != nil {
		fmt.Printf("Failed to analyze text: %v", err)
		return nil, err
	}

	float64SentimentScore := float64(sentiment.DocumentSentiment.Score)

	return &float64SentimentScore, nil
}

var languageClientOnce sync.Once
var languageClient *language.Client

// explicit reads credentials from the specified path.
func getLanguageClient(ctx context.Context, jsonPath, projectID string) (*language.Client, error) {
	languageClientOnce.Do(func() {
		var err error
		// Creates a client.
		languageClient, err = language.NewClient(ctx, option.WithCredentialsFile(jsonPath))
		if err != nil {
			fmt.Println("Failed to create client: " + err.Error())
		}
	})

	if languageClient == nil {
		var err error
		languageClient, err = getLanguageClient(ctx, jsonPath, projectID)
		if err != nil {
			fmt.Println("Failed to create client: " + err.Error())
		}
	}

	return languageClient, nil
}
