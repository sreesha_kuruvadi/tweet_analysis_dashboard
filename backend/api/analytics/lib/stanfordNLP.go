package lib

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"strconv"

	"github.com/hironobu-s/go-corenlp/connector"
)

var url = "http://localhost:9000"

//GetSentimentScoreStanfordNLP ...
func GetSentimentScoreStanfordNLP(ctx context.Context, text string) (*float64, error) {
	float64SentimentScore := 0.0

	c := connector.NewHTTPClient(context.Background(), url)
	c.Annotators = []string{"sentiment"}

	resp, err := c.Run(text)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	byteData, err := ioutil.ReadAll(resp)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	sentimentData := StanfordNLPRespone{}

	err = json.Unmarshal(byteData, &sentimentData)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	float64SentimentScore, err = strconv.ParseFloat(sentimentData.Sentences[0].SentimentValue, 64)
	if err != nil {
		return nil, err
	}

	// normalize the score
	float64SentimentScore = math.Mod(float64SentimentScore, 1.0)

	return &float64SentimentScore, nil
}

//StanfordNLPRespone ...
type StanfordNLPRespone struct {
	Sentences []Sentence `json:"sentences"`
}

//Sentence ...
type Sentence struct {
	SentimentValue        string    `json:"sentimentValue"`
	Sentiment             string    `json:"sentiment"`
	SentimentDistribution []float64 `json:"sentimentDistribution"`
}
