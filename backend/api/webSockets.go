package api

import (
	"fmt"
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
)

var upgraderOnce sync.Once
var connection *websocket.Conn

//Echo ...returns a web socket connection
func (s *Service) Echo(w http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	var err error

	var upgrader = websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}

	connection, err = upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
	}

	return connection, err
}
