package applicationservice

import "bitbucket.org/tweet_analysis_dashboard/backend/api"

//Service ...aggregation of all application dependencies
type Service struct {
	API *api.Service
}
