package applicationservice

import (
	"bitbucket.org/tweet_analysis_dashboard/backend/api"
)

//New ...creates a new application service which contains references to application based dependencies
func New(apiService *api.Service) *Service {
	return &Service{
		API: apiService,
	}
}
